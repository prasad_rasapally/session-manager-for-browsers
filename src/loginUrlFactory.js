import {inject} from 'aurelia-dependency-injection';
import uri from 'uri';
import LocationAdapter from './locationAdapter';
import SessionManagerConfig from './sessionManagerConfig';

/**
 * @class {LoginUrlFactory}
 */
@inject(LocationAdapter, SessionManagerConfig) class LoginUrlFactory {

    _locationAdapter:LocationAdapter;
    _config:SessionManagerConfig;

    constructor(locationAdapter:LocationAdapter,
                config:SessionManagerConfig) {

        if (!uri) {
            throw 'uri required';
        }

        if (!locationAdapter) {
            throw 'locationAdapter required';
        }
        this._locationAdapter = locationAdapter;

        if (!config) {
            throw 'config required';
        }
        this._config = config;

    }

    /**
     * Constructs a URL for initiating a SSO login flow and returning to the current page
     */
    construct() {

        const relayStateUrlHash =
            uri(
                uri(this._locationAdapter.href).fragment()
            )
                .addQuery('access_token={access_token}');

        const relayStateUrl =
            uri(this._locationAdapter.href).hash(`#${relayStateUrlHash.readable()}`);

        return uri(this._config.loginUrl)
            .addQuery('RelayState', relayStateUrl)
            .href();

    }

}

export default LoginUrlFactory;