import {inject} from 'aurelia-dependency-injection';
import SessionManagerConfig from './sessionManagerConfig';
import LocationAdapter from './locationAdapter';
import StorageAdapter from './storageAdapter';

/**
 * @class {LogoutFeature}
 */
@inject(SessionManagerConfig, LocationAdapter, StorageAdapter) class LogoutFeature {

    _config:SessionManagerConfig;
    _locationAdapter:LocationAdapter;
    _storageAdapter:StorageAdapter;

    constructor(config:SessionManagerConfig,
                locationAdapter:LocationAdapter,
                storageAdapter:StorageAdapter) {

        if (!locationAdapter) {
            throw 'locationAdapter required';
        }
        this._locationAdapter = locationAdapter;

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!storageAdapter) {
            throw 'storageAdapter required';
        }
        this._storageAdapter = storageAdapter;

    }

    /**
     * Logs out the currently logged in user by:
     * 1) removing their accessToken from localStorage
     * 2) redirecting them to the configured logoutUrl
     */
    execute() {

        this._storageAdapter.setAccessToken(null);

        // redirect to logoutUrl
        this._locationAdapter.href = this._config.logoutUrl;

    }

}

export default LogoutFeature;