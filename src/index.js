/**
 * @module
 * @description session manager public API
 */
export {default as SessionManagerConfig} from './sessionManagerConfig'
export {default as default} from './sessionManager';