import LoginUrlFactory from '../../src/loginUrlFactory';
import SessionManagerConfig from '../../src/sessionManagerConfig';

describe('LoginUrlFactory class', () => {
    describe('construct method', () => {
        it('includes current location hash parameters in RelayState', () => {
            /*
             arrange
             */
            // note we're using a quick & dirty trick that (at time of writing), params are spit out in
            // lexical order & aaa < access_token.
            const currentLocation = 'https://localhost/#/some-path?aaa=value';
            const fakeLocationAdapter = {href: `${currentLocation}`};
            const fakeConfig = {loginUrl: 'https://someloginurl.com/?123'};
            const expectedRelayState = encodeURIComponent(`${currentLocation}&access_token={access_token}`);
            const expectedUrl = `${fakeConfig.loginUrl}&RelayState=${expectedRelayState}`;
            const objectUnderTest = new LoginUrlFactory(fakeLocationAdapter, fakeConfig);

            /*
             act
             */
            const actualUrl = objectUnderTest.construct();

            /*
             assert
             */
            expect(actualUrl).toEqual(expectedUrl);

        });
        it('includes current location hash path in RelayState', () => {
            /*
             arrange
             */
            const currentLocation = 'https://localhost/#/some-path';
            const fakeLocationAdapter = {href: `${currentLocation}`};
            const fakeConfig = {loginUrl: 'https://someloginurl.com/?123'};
            const expectedRelayState = encodeURIComponent(`${currentLocation}?access_token={access_token}`);
            const expectedUrl = `${fakeConfig.loginUrl}&RelayState=${expectedRelayState}`;
            const objectUnderTest = new LoginUrlFactory(fakeLocationAdapter, fakeConfig);

            /*
             act
             */
            const actualUrl = objectUnderTest.construct();

            /*
             assert
             */
            expect(actualUrl).toEqual(expectedUrl);

        });
        it('includes hash separator in RelayState when not present in current location', () => {
            /*
             arrange
             */
            const currentLocation = 'https://localhost/';
            const fakeLocationAdapter = {href: `${currentLocation}`};
            const fakeConfig = {loginUrl: 'https://someloginurl.com/?123'};
            const expectedRelayState = encodeURIComponent(`${currentLocation}#?access_token={access_token}`);
            const expectedUrl = `${fakeConfig.loginUrl}&RelayState=${expectedRelayState}`;
            const objectUnderTest = new LoginUrlFactory(fakeLocationAdapter, fakeConfig);

            /*
             act
             */
            const actualUrl = objectUnderTest.construct();

            /*
             assert
             */
            expect(actualUrl).toEqual(expectedUrl);

        })
    });
});
